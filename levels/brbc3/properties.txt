{
;==============================================================================
;  RVGL LEVEL PROPERTIES
;
;  This file allows modification of these level properties:
;  - Surface materials
;  - Dust (graphical effects for materials)
;  - Corrugation (material bump effects)
;  - Particles (sparks of material effects and other objects)
;  - Trails of particles (like those of fireworks)
;  - Gravity (modify the behavior of the global gravity field)
;  - Pickups (modify pickup spawning and visual parameters)
;==============================================================================


;==============================================================================
;  MATERIALS
;==============================================================================
;  Default material IDs:
;    NONE:       -1  |  BUMPMETAL:         13
;    DEFAULT:     0  |  PEBBLES:           14
;    MARBLE:      1  |  GRAVEL:            15
;    STONE:       2  |  CONVEYOR1:         16
;    WOOD:        3  |  CONVEYOR2:         17
;    SAND:        4  |  DIRT1:             18
;    PLASTIC:     5  |  DIRT2:             19
;    CARPETTILE:  6  |  DIRT3:             20
;    CARPETSHAG:  7  |  ICE2:              21
;    BOUNDARY:    8  |  ICE3:              22
;    GLASS:       9  |  WOOD2:             23
;    ICE1:       10  |  CONVEYOR_MARKET1:  24
;    METAL:      11  |  CONVEYOR_MARKET2:  25
;    GRASS:      12  |  PAVING:            26
;==============================================================================
;==============================================================================

MATERIAL {
  ID              18                            ; Material ID [0 - 63]
  Name            "DIRT1"                       ; Display name
  Color           135 99 73                     ; Display color

  Skid            true                         ; Skidmarks appear on material
  Spark           false                         ; Material emits particles
  OutOfBounds     false                         ; Not implemented
  Corrugated      true                          ; Material is bumpy
  Moves           false                         ; Moves like museum conveyors
  Dusty           true                          ; Material emits dust

  Roughness       0.500000                      ; Roughness of the material
  Grip            50.000000                      ; Grip of the material
  Hardness        0.000000                      ; Hardness of the material

  DefaultSound    -1                            ; Sound when driving
  SkidSound       7                             ; Sound when skidding
  ScrapeSound     5                             ; Car body scrape [5:Normal]

  SkidColor       135 99 73                   ; Color of the skidmarks
  CorrugationType 5                             ; Type of bumpiness
  DustType        4                             ; Type of dust
  Velocity        0.000000 0.000000 0.000000    ; Move cars
}

	MATERIAL {
		ID				12
		Name			"LIGHT GRASS"
		Color			50 205 50

		Skid			false
		Spark			true
		OutOfBounds		false							; Not implemented
		Corrugated		true							; Material is bumpy
		Moves			false							; Moves like museum conveyors
		Dusty			true							; Material emits dust

		Roughness		0.500000						; Roughness of the material
		Grip			10.000000						; Grip of the material
		Hardness		0.500000						; Hardness of the material

		DefaultSound	-1								; Sound when driving
		SkidSound		7								; Sound when skidding
		ScrapeSound		5								; Car body scrape [5:Normal]

		SkidColor		50 205 50						; Color of the skidmarks
		CorrugationType	5 								; Type of bumpiness
		DustType		3								; Type of dust
		Velocity		0.000000 0.000000 0.000000		; Move cars
	}

;==============================================================================
;  CORRUGATION TYPES
;==============================================================================
;  Default corrugation IDs:
;    NONE:     0  |  CONVEYOR:  4
;    PEBBLES:  1  |  DIRT1:     5
;    GRAVEL:   2  |  DIRT2:     6
;    STEEL:    3  |  DIRT3:     7
;==============================================================================
;==============================================================================

;==============================================================================
;  DUST TYPES
;==============================================================================
;  Default dust IDs:
;    NONE:    0  |  GRASS:  3
;    GRAVEL:  1  |  DIRT:   4
;    SAND:    2  |  ROAD:   5
;==============================================================================
;==============================================================================

DUST {
  ID              4                             ; Dust ID [0 - 31]
  Name            "DIRT"                        ; Display name

  SparkType       -1                            ; Particle to emit
  ParticleChance  0.000000                      ; Probability of a particle
  ParticleRandom  0.000000                      ; Probability variance

  SmokeType       28                            ; Smoke particle to emit
  SmokeChance     0.200000                      ; Probability of a smoke part.
  SmokeRandom     0.800000                      ; Probability variance
}

;==============================================================================
;  SPARKS
;==============================================================================
;  Default spark IDs:
;    NONE:     -1  |  SMOKE1:       10  |  SPRINKLER:      21
;    SPARK:     0  |  SMOKE2:       11  |  SPRINKLER_BIG:  22
;    SPARK2:    1  |  SMOKE3:       12  |  DOLPHIN:        23
;    SNOW:      2  |  BLUE:         13  |  DOLPHIN_BIG:    24
;    POPCORN:   3  |  BIGBLUE:      14  |  SPARK3:         25
;    GRAVEL:    4  |  SMALLORANGE:  15  |  ROADDUST:       26
;    SAND:      5  |  SMALLRED:     16  |  GRASSDUST:      27
;    GRASS:     6  |  EXPLOSION1:   17  |  SOILDUST:       28
;    ELECTRIC:  7  |  EXPLOSION2:   18  |  GRAVELDUST:     29
;    WATER:     8  |  STAR:         19  |  SANDDUST:       30
;    DIRT:      9  |  PROBE_SMOKE:  20  |
;==============================================================================
;==============================================================================

SPARK {
  ID              28                            ; Particle ID [0 - 63]
  Name            "SOILDUST"                    ; Display name

  CollideWorld    true                          ; Collision with the world
  CollideObject   true                          ; Collision with objects
  CollideCam      false                         ; Collision with camera
  HasTrail        false                         ; Particle has a trail
  FieldAffect     false                         ; Is affected by force fields
  Spins           true                          ; Particle spins
  Grows           true                          ; Particle grows
  Additive        true                          ; Draw particle additively
  Horizontal      false                         ; Draw particle flat

  Size            36.000000 36.000000             ; Size of the particle
  UV              0.000000 0.000000             ; Top left UV coordinates
  UVSize          0.250000 0.250000             ; Width and height of UV
  TexturePage     47                            ; Texture page
  Color           38 35 6                      ; Color of the particle

  Mass            0.030000                      ; Mass of the particle
  Resistance      0.002000                      ; Air resistance
  Friction        0.000000                      ; Sliding friction
  Restitution     0.000000                      ; Bounciness

  LifeTime        0.500000                      ; Maximum life time
  LifeTimeVar     0.100000                      ; Life time variance

  SpinRate        0.000000                      ; Avg. spin rate (radians/sec)
  SpinRateVar     6.000000                      ; Variation of the spin rate

  SizeVar         2.000000                      ; Size variation
  GrowRate        120.000000                      ; How quickly it grows
  GrowRateVar     20.000000                     ; Grow variation

  TrailType       -1                            ; ID of the trail to use
}

;==============================================================================
;  TRAILS
;==============================================================================
;  Default trail IDs:
;    NONE:     -1  |  SPARK:  1
;    FIREWORK:  0  |  SMOKE:  2
;==============================================================================
;==============================================================================

;==============================================================================
;  GRAVITY
;==============================================================================

;==============================================================================
;  WIND
;==============================================================================

;==============================================================================
;  PICKUPS
;==============================================================================

	PICKUPS {
		SpawnCount      17 1                           ; Initial and per-player count
		EnvColor        255 255 128                   ; Color of shininess (RGB)
		LightColor      128 96 0                      ; Color of light (RGB)
	}
}