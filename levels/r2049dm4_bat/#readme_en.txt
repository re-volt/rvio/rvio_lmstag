﻿═══╦═══════╦═══
   ║SUMMARY║
   ╚═══════╝

Name:			R2049 Atomic Battle
Folder Name:		r2049dm4_bat
Author:			Dyspro50
Category:		Extreme Conversion (San Francisco Rush 2049)
Original release date:	December 23, 2016

Tools (from the circuit version):
			MAKEITGOOD
			NGCGeoAPRM (home-maded converter)
			Blender 2.73 with Jigebren's Re-Volt Plugin (v2015-02-27)
			rvtmod8
			rvglue
			PaintDotNet


═══╦═════════════════╦═══
   ║TRACK DESCRIPTION║
   ╚═════════════════╝

"Atomic" ("Battle 4" in N64 North-American release) is one of the 8 battle areas found in console versions of San Francisco Rush 2049. This track is part of the "Re-Volt X Rush 2049" long-term project.

Includes original DC/MAT3 music, "Robo" by Barry Leitch.


═══╦════════════╦═══
   ║KNOWN ISSUES║
   ╚════════════╝

None so far...


═══╦═══════╦═══
   ║UPDATES║
   ╚═══════╝

2023-07-08 (From the circuit version)
	- Included I/O Pack fixes done by MightyCucumber/SantiTM and others, which include the following:
		- Fixed floor collision issue in the blue section
		- Added collision to the upper part of the track
		- BMP to PNG conversion of textures and track gfx
		- Reduced unnecessary 512px wide track gfx to 256px


═══╦═════╦═══
   ║NOTES║
   ╚═════╝

- This track requires RVGL 15.1220a or higher

- Meshes DEPLIGNE.prm, MINIRAMP.prm and XBLOQ.prm done by Dyspro50. The rest is entirely done by Rush 2049 artists, excluding very minor textures and meshing tweaks.

	- You are allowed to reuse any files only if you credit their respective author(s).

- If you have any questions, please contact me on The Re-Volt Hideout, or by e-mail at dyspro50@gmail.com


═══╦═════════════════╦═══
   ║SPECIAL THANKS TO║
   ╚═════════════════╝

- Midway and Atari Games for this wonderful game and for the original mesh!

- All Re-Volters who support the project

- All Re-Volters who contributed to the track fixes

- You for downloading this track!



 Dyspro


********************************************************************************************************
