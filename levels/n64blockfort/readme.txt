This is a replica of Block Fort, a Battle Mode track from Mario Kart 64 ( and sequels featuring retro tracks). 
The star will spawn on top of one of four forts, while pickups are scattered across the base of the fort.

The music is a "remix" of the theme used in Battle Mode.

The custom folder was lifted from DC.All's Donut Plains 3. I couldn't find a readme or anything explicitly mentioning if that's okay or not, so please tell me if you'd like to have them removed or somesuch.
Big thanks for Marv for the final touches ( fixing collisions, adding shadows, re-doing the vertex painting, re-packaging, etc.)

Block Fort, Mario Kart, Donut Plains, and all other related content is property of Nintendo. Something something copyrights. Don't copy that floppy, you wouldn't download a car.