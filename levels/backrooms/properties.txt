PICKUPS {
  SpawnCount      64 64                           ; Initial and per-player count
  EnvColor        255 255 255                   ; Color of shininess (RGB)
  LightColor      0 0 0                      ; Color of light (RGB)
}