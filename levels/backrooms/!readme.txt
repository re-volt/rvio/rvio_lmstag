
██████╗░░█████╗░░█████╗░██╗░░██╗██████╗░░█████╗░░█████╗░███╗░░░███╗░██████╗
██╔══██╗██╔══██╗██╔══██╗██║░██╔╝██╔══██╗██╔══██╗██╔══██╗████╗░████║██╔════╝
██████╦╝███████║██║░░╚═╝█████═╝░██████╔╝██║░░██║██║░░██║██╔████╔██║╚█████╗░
██╔══██╗██╔══██║██║░░██╗██╔═██╗░██╔══██╗██║░░██║██║░░██║██║╚██╔╝██║░╚═══██╗
██████╦╝██║░░██║╚█████╔╝██║░╚██╗██║░░██║╚█████╔╝╚█████╔╝██║░╚═╝░██║██████╔╝
╚═════╝░╚═╝░░╚═╝░╚════╝░╚═╝░░╚═╝╚═╝░░╚═╝░╚════╝░░╚════╝░╚═╝░░░░░╚═╝╚═════╝░

+++++++
Credits
+++++++

Original track model: https://sketchfab.com/3d-models/backrooms-another-level-429f3c9ea8024f5e9bb78f6649c7bd26
Empty models and souns are taken from Kiwi's Schizophrenia.

+++++
About
+++++

The track is dedicated to the Backrooms. 
The Backrooms - urban legend and creepypasta describing an endless maze randomly generated office rooms and other environments. 
It is characterized by the smell of moist carpet, walls with a monochromatic tone of yellow, and buzzing fluorescent lights.
Internet users have expanded upon this concept by creating different "levels" of the Backrooms and "entities" which inhabit them.

The text is taken from Wikipedia.

+++++++
Contact
+++++++

Discord: rodik#1750
YT: rodik