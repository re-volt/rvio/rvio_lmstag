MODEL   0   "notes"

ANIMATION {
  Slot                      0
  Name                      "Piano Roll"
  Mode                      0

  BONE {
    BoneID                  0
    ModelID                 0
  }

  KEYFRAME {
    FrameNr                 0
    Time                    3
    Type                    0
    BONE {
      BoneID                0
      RotationAxis          1.000 0.000 0.000
      RotationAmount        360.000
    }
  }
}