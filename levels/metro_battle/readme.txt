Metro Battle by Zeino (Tryxn on RVZ)

this is a battle tag arena that uses custom animations
its compact and should be really fun in online or split screen fun!
enjoy! please let me know if you would like to see a race version of this ;)

credits:
textures - Spider-Man The Movie game (models-resource), Schools Out! by Kiwi, me and internet
sounds - freesound.org
music - waterflame - streetwise
metro model - remodel of metro model used in Metro-Volt by human
other models - Schools Out! by Kiwi
everything under cc