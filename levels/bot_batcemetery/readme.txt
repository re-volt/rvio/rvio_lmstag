-*--Cemetery Battle--*---by hilaire9  ---June 2002--      (now revolt 1.2 ready)       
----Type= Battle                ---Length= N/A  
----Zip name= 'cemetery_battle' ---Size= 1.4Mb 

*-Replaces Garden Battle (select Garden Battle after install)

***To play Cemetery Battle you must replace Garden Battle
   with Cemetery Battle, thus: 

**Step 1: Unzip into the main Re-Volt folder. This installs
          a folder named: bot_batCemetery in the levels folder.

**Step 2: Go to the levels folder.  Rename the bot_bat 
          folder to: bot_batBackup. Then rename bot_batCemetery 
          to bot_bat. You can now to play Cemetery Battle. To get      
          Garden Battle back simply rename to folders back to the
          way they were. 

----Description: My Cemetery track converted to a Battle track.                              
---*Credit: JimK (kits), BurnRubr, Ekla, Antimorph & Scloink,
            Re-volt, and the Web for various prms and textures.
            Last_Cuban for a test battle.  
---Tools: Re-Volt Track Editor, Ali's rvGlue5, Adobe 
          PhotoDeluxe, MS Paint, and JimK's level Renamer.
                             
              




