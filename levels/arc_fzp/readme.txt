********************************************************************************
**                               Changelog                                    **
********************************************************************************
--------------------------------------------------------------------------------
Version 23.1011a

- Bugfix release

# Roof has become steeper so it can't be exploited. (EstebanMz)

--------------------------------------------------------------------------------
Version 20.1222a

- Bugfix release

# More friendly to go upstairs, the stairs are replaced by ramps (Javildesign)

--------------------------------------------------------------------------------
Version 20.0929a

- Bugfix release

# Raised the walls to ridiculous heights. (MightyCucumber)
# Added Collision Wood Roof. (MightyCucumber)

--------------------------------------------------------------------------------

Version 20.0919a

- First Alpha release

--------------------------------------------------------------------------------
Name: Frozen Pit
Release date: 2020
Track Type: Battle Tag (Multiplayer only)
Difficulty: Extreme
--------------------------------------------------------------------------------
Notes: Batte Tag Arena originally made for the canceled Arctic Blast Mod.
Base and original arena is made by Zach.
Custom sky and snow effects is made by Xarc.
GFX photo is made by Santiii727.
Bugfixes is made by MightyCucumber and Javildesign.

Special Notes:
Thanks to Zach for share this and MightyCucumber for contact and contributing this map to the re-volt I/O Workshop.

Contact Zach:  zach@revoltzone.net


©️ 2009-2020 Zach and RVGL I/O Community.
