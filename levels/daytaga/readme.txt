********************************************************************************
**                               Changelog                                    **
********************************************************************************
--------------------------------------------------------------------------------
Version 23.1010a

- Bugfix release

# Fence removed. (EstebanMz)
# Added missing collisions to the ambulance and other surrounding objects. (EstebanMz)
# Fog Distance increased. (EstebanMz)

--------------------------------------------------------------------------------
Version 20.0919a

- Bugfix release

# Added the invisible walls to ridiculous heights. (MightyCucumber)

--------------------------------------------------------------------------------
- Original Release

DAYTAGA 500
By DSL_Tile & SuperTard

Original Concept              DSL_Tile
markar.w (level visual)      SuperTard     
markar.ncp (level collision) SuperTard
.bmp (texturemaps)            SuperTard
.cam (cameras)                SuperTard, Yamada, Rex
.fan (car AI)                 DSL_Tile
.fin (instances)              SuperTard
.fld (farce fields)           SuperTard
.fob (objects)                SuperTard
.inf (level info)             DSL_Tile, T6s, SuperTard
.lit (lights)                 SuperTard
.ncp (instance collision)     SuperTard
.pan (position nodes)         DSL_Tile
.taz (trackzones)             DSL_Tile
.tri (triggers)               SuperTard
.vis (visiboxes)              SuperTard, tweaked by T6s

Additional Credits to:       TheMe&theMe for use of NovBos car
			     (novbos original mesh by CADster)
			     Ali & Antimorph for ase2w tools
			     LaserBeams for use of his Ambulance
                             Everyone from www.zone.com/revolt/
			     Everyone from www.racerspoint.com
			     Everyone from www.rvarchive.com
			     Everyone from #revolt-chat on austnet.org,
			     especially beta testers for their input			     
			     & anyone who still plays Re-Volt :)

*Description*
--------------
This track is based on a sketch by DSL_Tile.  The similar layout to
Daytona International Raceway soon became apparent, but is by no means
a scale or semi-scale mock-up.

Also be sure to get the soon released Nascar cars 
from the RVCC team, they will fit the track well.


* Tips/Gameplay *
--------

First person to get the star is "it".  If you are "it" you
will glow green till someone touches you.  First person to
stay "it" for 2 minutes wins. Battle-tag is a multi-player
game "option".  If you have no-one to play multi with, can
start a "fake" multiplayer game as the HOST then hit tab
in the "waiting for players screen", if you want to
practice a long time, avoid the star.

If you would like to disable the boobies feature:
go to options-video-rendersettings & disable the instances.
(only the novbos will disappear)

* Copyright/Permissions *
-------------------------
Not to be reproduced or sold without specific permission from authors:
DSL_Tile:  atileman@swbell.net
Supertard: brgpug@ev1.net
All corporate logos are copyright & property of respective owners.