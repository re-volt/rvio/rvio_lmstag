{
MODEL   0   "hour"
MODEL   1   "minute"

ANIMATION {
  Slot                      0                        
  Name                      "Hour"         
  Mode                      0                         

  BONE {
    BoneID                  0                     
    ModelID                 0                     
  }

  KEYFRAME {
    FrameNr                 0                        
    Time                     43200                    
    Type                    0                        

    BONE {
      BoneID                0                     
      RotationAxis          0.000 0.000 1.000      
      RotationAmount        -360.000                  
    }
   }
}

ANIMATION {
  Slot                      1                        
  Name                      "Minute"         
  Mode                      0                         

  BONE {
    BoneID                  0                     
    ModelID                 1                     
  }

  KEYFRAME {
    FrameNr                 0                        
    Time                    3600                    
    Type                    0                        

    BONE {
      BoneID                0                     
      RotationAxis          0.000 0.000 1.000      
      RotationAmount        -360.000                  
    }
   }
}



}