____________________________________________________
**IMPORTANT**
THIS LEVEL REQUIRES RVGL 20.0430a (or higher) TO RUN. 

____________________________________________________

HOSPITAL BATTLE

Game Type: Battle Tag
Length: n/a
Difficulty: n/a

Created: 04/03/2023


____________________________________________________

Description:

A sprawling hospital with multi-leveled action. A variety of vertical and flanking routes make this a fun find and chase challenge.


____________________________________________________

Developers:

GJ
-Models
-Textures
-Objects
-Animations
-Lighting and Shading
-Triggers and Cameras
-POS Nodes
-AI Nodes
-Visiboxes

Mighty Cucumber
-Original Theme Pitch and Research
-Early Raceline Build
-General Feedback/Direction

I Spy
-Color Design and Pallette
-Visual Design Advisor
-Animation Advisor
-AI Nodes Advisor
-Sound Curation

Music is Opressive Sphere by ZetaSphere (Edited for brevity)
Techincian computer sounds borrowed from https://www.youtube.com/watch?v=JpSfgusep7s

Made with Blender Plugin

____________________________________________________

Stats:

World Faces: 9,930
NCP Faces: 1,718
Development Time: 9 days


____________________________________________________

Patch - 8/25/2024

-Vertex color errors under counters and doorframes fixed
-Fog distance reduced in lower performance modes
-Reposition system created at elevators to catch cheaters trying to climb down the shaft
-Instance priorities added for low performance mode
-Renamed propane.prm to oxygen.prm (it was literally unplayable)  

____________________________________________________



Trivia:

-The battle arena borrowed some content that got cut from the main race tracks, such as: Ambulance Garage, Incubation Ward, Gym.
-Some of the layout was inspired by the early mesh built by Mighty Cucumber for the race tracks
-The technicians room features an MRI animation of the brain. It's GJs brain. That's GJs brain on the screen. GJ got an MRI and it was loud.
-The elevators are able to be ridden in, a feature that didn't make it in an alternate path in Hospital 2, which instead repositioned you to keep the race moving.
-The exercise ball is a custom-textured beachball. We finally figured out how to make one.

____________________________________________________
IMPORTANT
-Items in this directory are built and designed by GJ and I SPY.
-Please consult GJ before altering or reusing assets outside of personal use. (I'm lenient about asset use, don't be afraid to ask)
-Feel free to distribute as is without taking credit from the developers.
-Do not alter this Readme file, and include it in this directory if distributing to friend, family, or whomever. 
-This track directory is free to download and use, it is not for sale and not to be sold. Period.

Re-Volt Hideout
https://forum.re-volt.io/memberlist.php?mode=viewprofile&u=2090

Email (I don't check this too often)
grnthn2@gmail.com

Discord
.?#8017  (replace ? with coffee mug emoji)

I SPY is reclusive, but if all else fails you can try to contact him instead. He also isn't a spy that's just his name.




We hope you thoroughly enjoy this track,
Thank you  :)