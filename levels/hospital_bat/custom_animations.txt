{
MODEL   0   "elevator"
MODEL   1   "door1_l"
MODEL   2   "door1_r"
MODEL   3   "door2_l"
MODEL   4   "door2_r"
SFX        0   "liftbell"
SFX        1   "open"
SFX        2   "close"


ANIMATION {
  Slot                      0                        
  Name                      "Elevator"         
  Mode                      1   
  NeedsTrigger              true                    
  TriggerOnce	false  

  BONE {
    BoneID                  0                     
    ModelID                 -1                     
  }
  BONE {
    BoneID                  1                     
    ModelID                 0                     
  }
  BONE {
    BoneID                  2                    
    ModelID                 1     
   Parent                     0  
   OffsetTranslation       0.000 0.000 365.000              
  }
  BONE {
    BoneID                  3                    
    ModelID                 2          
   Parent                     0      
   OffsetTranslation       0.000 0.000 -365.000        
  }
  BONE {
    BoneID                  4                    
    ModelID                 3  
   Parent                     1  
   OffsetTranslation       0.000 0.000 365.000                    
  }

  BONE {
    BoneID                  5                    
    ModelID                 4       
   Parent                     1   
   OffsetTranslation       0.000 0.000 -365.000            
  }

  BONE {
    BoneID                  6                    
    ModelID                 1       
   Parent                     0  
   OffsetTranslation       0.000 -2402.77 0.000            
  }

  BONE {
    BoneID                  7                    
    ModelID                 2       
   Parent                     0
   OffsetTranslation       0.000 -2402.77 0.000  
  }



;  IDLE START
  KEYFRAME {  
    FrameNr                 0                        
    Time                    0                    
    Type                    0    
   }

;  IDLE BOTH OPEN
  KEYFRAME {  
    FrameNr                 1                        
    Time                    0.5                    
    Type                    0     
    BONE {
      BoneID                1                               
    }      

   }

;DOOR FRONT CLOSE 1
  KEYFRAME {  
    FrameNr                 2                        
    Time                    0.5                    
    Type                    4                        
  BONE {
    BoneID                  1                     
      Translation           0.000 0.000 0.000
      SFX {
        SfxID                 2
        Range               10
        Looping             false
      }                  
  }       
    BONE {
      BoneID                2                     
      Translation           0.000 0.000 -65.000             
    }
    BONE {
      BoneID                3                     
      Translation           0.000 0.000 65.000             
    }
    BONE {
      BoneID                4                     
      Translation           0.000 0.000 -65.000             
    }
    BONE {
      BoneID                5                     
      Translation           0.000 0.000 65.000            
    }
   }
;DOOR FRONT CLOSE 2
  KEYFRAME {  
    FrameNr               3                        
    Time                    3                    
    Type                    1                        

    BONE {
      BoneID                2                     
      Translation           0.000 0.000 -300.000             
    }
    BONE {
      BoneID                3                     
      Translation           0.000 0.000 300.000             
    }
    BONE {
      BoneID                4                     
      Translation           0.000 0.000 -300.000             
    }
    BONE {
      BoneID                5                     
      Translation           0.000 0.000 300.000            
    }
   }


;ELEVATOR ASCEND
  KEYFRAME {  
    FrameNr                 4                        
    Time                    8                    
    Type                    3                        

    BONE {
      BoneID                1                     
      Translation           0.000 -2402.77 0.000             
    }
   }
;UPPER DOOR OPEN 
  KEYFRAME {  
    FrameNr                5                        
    Time                    2                    
    Type                    3       
                
    BONE {
      BoneID                1    
      SFX {
        SfxID                 0
        Range               10
        Looping             false
      }                              
}
    BONE {
      BoneID                4   
      Translation           0.000 0.000 365.000                          
}
    BONE {
      BoneID                5  
      Translation           0.000 0.000 -365.000                              
}

    BONE {
      BoneID                6                     
      Translation           0.000 0.000 365.000               
      SFX {
        SfxID                 1
        Range               10
        Looping             false
      }             
    }

    BONE {
      BoneID                7                     
      Translation           0.000 0.000 -365.000                
    }
   }

;UPPER IDLE 
  KEYFRAME {  
    FrameNr                6                        
    Time                    5                    
    Type                    3                        
   }

;UPPER DOOR CLOSE 1
  KEYFRAME {  
    FrameNr                 7                        
    Time                    0.5                    
    Type                    4                        

    BONE {
      BoneID                4   
      Translation           0.000 0.000 -65.000                          
}
    BONE {
      BoneID                5  
      Translation           0.000 0.000 65.000                              
}

    BONE {
      BoneID                6                     
      Translation           0.000 0.000 -65.000               
      SFX {
        SfxID                 2
        Range               10
        Looping             false
      }             
    }

    BONE {
      BoneID                7                     
      Translation           0.000 0.000 65.000                
    }
   }
;UPPER DOOR CLOSE 2
  KEYFRAME {  
    FrameNr               8                        
    Time                    3                    
    Type                    1                        

    BONE {
      BoneID                4   
      Translation           0.000 0.000 -300.000                          
}
    BONE {
      BoneID                5  
      Translation           0.000 0.000 300.000                              
}

    BONE {
      BoneID                6                     
      Translation           0.000 0.000 -300.000                          
    }

    BONE {
      BoneID                7                     
      Translation           0.000 0.000 300.000                
    }
   }

;ELEVATOR DESCEND
  KEYFRAME {  
    FrameNr                 9                        
    Time                    8                    
    Type                    3                        

    BONE {
      BoneID                1                     
      Translation           0.000 2402.77 0.000           
    }
   }

;DOOR OPEN 
  KEYFRAME {  
    FrameNr                 10                        
    Time                    2                    
    Type                    3       
                 
  BONE {
    BoneID                  1                     
      Translation           0.000 0.000 0.000
      SFX {
        SfxID                 0
        Range               10
        Looping             false
      }         
  }             
    BONE {
      BoneID                2                     
      Translation           0.000 0.000 365.000             
      SFX {
        SfxID                 1
        Range               10
        Looping             false
      }                 
    }
    BONE {
      BoneID                3                     
      Translation           0.000 0.000 -365.000             
    }
    BONE {
      BoneID                4                     
      Translation           0.000 0.000 365.000             
    }
    BONE {
      BoneID                5                     
      Translation           0.000 0.000 -365.000            
    }
    BONE {
      BoneID                6                     
      Translation           0.000 0.000 0.000         
    }

    BONE {
      BoneID                7                     
      Translation           0.000 0.000 0.000             
   }

}
;  IDLE FINISH
  KEYFRAME {  
    FrameNr                 11                        
    Time                    6                    
    Type                    0    
   }


}